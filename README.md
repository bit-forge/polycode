# Polycode

[https://bit-forge.gitlab.io/polycode](https://bit-forge.gitlab.io/polycode)

[DockerHub image: polycode-dev](https://hub.docker.com/r/orgbitforge/polycode-dev)

[docker-f]: https://img.shields.io/badge/Docker-003f8c.svg?style=flat&logo=docker&logoColor=fff&cacheSeconds=3600
[dockerhub-f]: https://img.shields.io/badge/DockerHub-2496ED.svg?style=flat&logo=docker&logoColor=fff&cacheSeconds=3600
[vite-f]: https://img.shields.io/badge/Vite-1b1b1f.svg?style=flat&logo=vite&logoColor=646cff&cacheSeconds=3600
[react-f]: https://img.shields.io/badge/React-23272F.svg?style=flat&logo=react&logoColor=61DAFB&cacheSeconds=3600
[npm-f]: https://img.shields.io/badge/npm-fff.svg?style=flat&logo=npm&logoColor=cb3837&cacheSeconds=3600
[vs-code-f]: https://img.shields.io/badge/VS%20Code-2c2c32.svg?style=flat&logo=visualstudiocode&logoColor=22a8f1&cacheSeconds=3600
[git-f]: https://img.shields.io/badge/Git-000.svg?style=flat&logo=git&logoColor=f54d27&cacheSeconds=3600
[rescript-f]: https://img.shields.io/badge/ReScript-fff.svg?style=flat&logo=rescript&logoColor=e6484f&cacheSeconds=3600
[tailwindcss-f]: https://img.shields.io/badge/Tailwind_CSS-0b1120.svg?style=flat&logo=tailwindcss&logoColor=06b6d4&cacheSeconds=3600

Built with:

![vite-f] ![react-f] ![rescript-f] ![tailwindcss-f]

Tools:

![docker-f] ![dockerhub-f] ![git-f] ![npm-f] ![vs-code-f]

## Development

Run dev

```sh
make dev
```

---

Build

```sh
make build
```
