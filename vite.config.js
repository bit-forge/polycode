import { defineConfig } from "vite"

export default defineConfig({
  base: "https://bit-forge.gitlab.io/polycode/",
  plugins: [],
  build: {
    outDir: "./public"
  }
})
