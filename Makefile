.PHONY: build dev
.SILENT: build dev

build:
	npm run build:res
	npm run build

dev:
	npm run dev
