@react.component
let make = (~header) => {
  <main className="flex flex-col h-full w-full">
    <Header />
    
    <div className="flex w-full flex-grow">
      <aside className="w-60 border-r border-gray-400">{React.string("Sidebar")}</aside>
      <main className="flex-grow">
        <h1> {React.string(header)} </h1>
      </main>
    </div>
  </main>
}
