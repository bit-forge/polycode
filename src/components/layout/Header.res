@react.component
let make = () => {
  <header className="flex flex-row align-middle justify-center border-b border-gray-400 bg-slate-300">
    <h1 className="text-4xl font-bold py-3">{React.string("Header")}</h1>
  </header>
}